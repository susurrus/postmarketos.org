# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later

imgs_url = "https://images.postmarketos.org/bpo"

# Both are without the service pack! Usually we don't generate new images after
# publishing a service pack, people are expected to install the service pack
# through update functionality. This means, we might have just announced a
# service pack, but the images don't include it yet.
latest_release = "v24.06"


# Add '"not_in_stable": True' if we only have pre-built images for edge.
table = {
    "Main": {
        "pine64-pinephone": {
            "name": "PINE64 PinePhone",
        },
        "purism-librem5": {
            "name": "Purism Librem 5",
        },
    },
    "Community": {
        "arrow-db410c": {
            "name": "Arrow DragonBoard 410c",
        },
        "asus-me176c": {
            "name": "ASUS MeMO Pad 7",
        },
        "bq-paella": {
            "name": "BQ Aquaris X5",
        },
        "fairphone-fp4": {
            "name": "Fairphone 4",
        },
        "generic-x86_64": {
            "name": "Generic x86_64",
        },
        "google-gru": {
            "name": "Google Gru Chromebooks",
        },
        "google-kukui": {
            "name": "Google Kukui Chromebooks",
        },
        "google-oak": {
            "name": "Google Oak Chromebooks",
        },
        "google-peach-pit": {
            "name": "Samsung Chromebook 2 11.6\"",
        },
        "google-snow": {
            "name": "Samsung Chromebook",
        },
        "google-trogdor": {
            "name": "Google Trogdor Chromebooks",
        },
        "google-veyron": {
            "name": "Google Veyron Chromebooks",
        },
        "google-x64cros": {
            "name": "Google Chromebooks with x64 CPU",
        },
        "lenovo-a6000": {
            "name": "Lenovo A6000",
        },
        "lenovo-a6010": {
            "name": "Lenovo A6010",
        },
        "lenovo-21bx": {
            "name": "Lenovo X13s",
        },
        "microsoft-surface-rt": {
            "name": "Microsoft Surface RT",
        },
        "motorola-harpia": {
            "name": "Motorola Moto G4 Play",
        },
        "nokia-n900": {
            "name": "Nokia N900",
        },
        "nvidia-tegra-armv7": {
            "name": "Nvidia Tegra armv7",
        },
        "odroid-xu4": {
            "name": "ODROID XU4",
        },
        "oneplus-enchilada": {
            "name": "OnePlus 6",
        },
        "oneplus-fajita": {
            "name": "OnePlus 6T",
        },
        "pine64-pinebookpro": {
            "name": "PINE64 PineBook Pro",
        },
        "pine64-pinephonepro": {
            "name": "PINE64 PinePhone Pro",
        },
        "pine64-rockpro64": {
            "name": "PINE64 RockPro 64",
        },
        "samsung-a3": {
            "name": "Samsung Galaxy A3 (2015)",
        },
        "samsung-a5": {
            "name": "Samsung Galaxy A5 (2015)",
        },
        "samsung-e7": {
            "name": "Samsung Galaxy E7",
        },
        "samsung-espresso10": {
            "name": "Samsung Galaxy Tab 2 10.1\"",
        },
        "samsung-espresso7": {
            "name": "Samsung Galaxy Tab 2 7.0",
        },
        "samsung-serranove": {
            "name": "Samsung Galaxy S4 Mini Value Edition",
        },
        "samsung-grandmax": {
            "name": "Samsung Galaxy Grand Max",
        },
        "samsung-gt510": {
            "name": "Samsung Galaxy Tab A 9.7 (2015)",
        },
        "samsung-gt58": {
            "name": "Samsung Galaxy Tab A 8.0 (2015)",
        },
        "samsung-m0": {
            "name": "Samsung Galaxy S III (GT-I9300/SHW-M440S)",
        },
        "samsung-manta": {
            "name": "Google Nexus 10",
        },
        "shift-axolotl": {
            "name": "SHIFT6mq",
        },
        "wileyfox-crackling": {
            "name": "Wileyfox Swift",
        },
        "xiaomi-beryllium": {
            "name": "Xiaomi Pocophone F1",
        },
        "xiaomi-daisy": {
            "name": "Xiaomi Mi A2 Lite",
        },
        "xiaomi-markw": {
            "name": "Xiaomi Redmi 4 Prime",
        },
        "xiaomi-mido": {
            "name": "Xiaomi Redmi Note 4",
        },
        "xiaomi-scorpio": {
            "name": "Xiaomi Mi Note 2",
        },
        "xiaomi-tissot": {
            "name": "Xiaomi Mi A1",
        },
        "xiaomi-vince": {
            "name": "Xiaomi Redmi 5 Plus",
        },
        "xiaomi-wt88047": {
            "name": "Xiaomi Redmi 2",
        },
        "xiaomi-ysl": {
            "name": "Xiaomi Redmi S2/Y2",
        },
    },
    "Testing": {
        "fairphone-fp5": {
            "name": "Fairphone 5",
        },
        "generic-x86_64": {
            "name": "Generic x86_64 EFI System",
        },
        "google-asurada": {
            "name": "Google Asurada Chromebooks",
            "not_in_stable": True,
        },
        "google-cherry": {
            "name": "Google Cherry Chromebooks",
            "not_in_stable": True,
        },
        "google-corsola": {
            "name": "Google Corsola Chromebooks",
            "not_in_stable": True,
        },
        "google-nyan-big": {
            "name": "Acer Chromebook 13 CB5-311",
        },
        "google-nyan-blaze": {
            "name": "HP Chromebook 14 G3",
        },
        "microsoft-surface-rt": {
            "name": "Microsoft Surface RT",
        },
    },
}
