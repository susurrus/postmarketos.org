title: "Call For Testers"
title-short: "call4testers"
date: 2023-05-21
preview: "2021-12/op6.jpg"
---
[#grid side#]
[![OnePlus 6 on the beach, showing a terminal](/static/img/2021-12/op6_thumb.jpg){: class="w200 border"}](/static/img/2021-12/op6.jpg)
[#grid text#]

[Alpine Linux 3.18](https://alpinelinux.org/posts/Alpine-3.18.0-released.html)
has been released recently, which means the next postmarketOS release v23.06
will be available
[shortly](https://wiki.postmarketos.org/wiki/Creating_a_release_branch#Timeline).

In preparation for the release we created a new testing team. This is our way
of getting the community directly involved into the testing process, which has
been mostly done by the developers before. If you are already a postmarketOS
user, consider joining the testing team to make sure that the new release runs
as good as possible on your particular phone! After the release, we plan to
make service packs available early to the testing team as well.

=> *[Read more about / join the testing team](https://wiki.postmarketos.org/wiki/Testing_Team)*

[#grid end#]

While they won't make it into the v23.06 release just yet, recently a lot of
[Chrome OS devices](https://wiki.postmarketos.org/wiki/Chrome_OS_devices)
were added to postmarketOS edge. A lot of those Chromebooks
are so similar that in theory they should work even though we were only able to
actually test on a few of them. If you happen to own one of these many
Chromebooks and would like to run postmarketOS with a mainline Linux kernel on
it: Give it a try, [write an issue](/issues) if it does not work, and consider
joining the testing team to get new changes first and make sure we catch
regressions for your particular device.

On the topic of testing, stability and support expectations, etc. we also have
a new [state of postmarketOS](/state) page.

That's it for today, stay tuned for the v23.06 release and have a nice day!
