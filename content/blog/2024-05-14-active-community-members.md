title: "Introducing \"Active Community Members\""
title-short: "Active Community Members"
date: 2024-05-14
---
The call for [Trusted Contributors](/blog/2023/12/03/how-to-become-tc/) we
put out in December (and [part 2](/blog/2024/01/28/how-to-become-tc2/)) has
been successful beyond our wildest imagination. As of writing we now have 11
TCs, who make a huge impact regarding the amount of patches we get into
postmarketOS and the issues that get answered. We have learned that a great way
to scale postmarketOS up is empowering well known community members to just do
what they care about in this project.

With today's blog post we want to cut some more red tape. Besides the Trusted
Contributors, we have a whole lot more active community members that are either
listed in
[CODEOWNERS](https://gitlab.com/postmarketOS/pmaports/-/blob/master/CODEOWNERS)
or just have been very active in the issue trackers.

## Permissions

We figured that it would be immensely useful if those very valued members of
our community would be able to:

* Triage issues by applying labels, and being able to close them.
* Approve merge requests to packages that they maintain.

The big difference to the Trusted Contributor role is, that Active Community
Members do not need to submit a formal application, and that they don't get
merge rights.

## How it will work

* You must have 2FA enabled on your GitLab account.

* If you have your name in the pmaports CODEOWNERS file and have been active on
  GitLab within the last three months, expect an invite to a new
  "postmarketOS Active Community Members" GitLab group over the coming weeks.

* If you are not in CODEOWNERS, but you do help out a bunch with pmaports issue
  triage (which should be obvious from your GitLab activity) and would like to
  do this more effectively, then you can also join this group.
  [Write a short e-mail](mailto:team@postmarketos.org), mentioning your
  gitlab nickname and that you would like to join the Active Community
  Maintainers.

* The new Active Community Members group gets
  [reporter](https://docs.gitlab.com/ee/user/permissions.html) permissions on
  the [pmaports](https://wiki.postmarketos.org/wiki/Pmaports.git) repository.

Improving the triaging process itself is currently being discussed in
{{issue|60|postmarketos}} and further feedback is appreciated. The current
iteration of labels and how they are meant to be used can be found in the
[triaging](https://wiki.postmarketos.org/wiki/Triaging) wiki article.

## Groups overview

The groups within postmarketOS are now the Core Team, Trusted Contributors,
Active Community Members and the Testing Team. While it may sound like a bit
much, having this organizational structure really helps to stay on top of the
issue tracker and get relevant changes into postmarketOS faster. A homepage
update is in the works, that will have a quick overview of all groups in a new
*team* page.
