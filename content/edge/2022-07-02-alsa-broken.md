title: "ALSA 1.2.7 update breaks audio on multiple platforms"
date: 2022-07-02
---

A recent update to the ALSA package (1.2.7) in Alpine Edge has broken audio
on multiple platforms:

- PINE64 PineTab
- PINE64 Pinebook Pro
- PINE64 Pinephone
- PINE64 Pinephone Pro
- Purism Librem 5
- potentially others not identified yet...

UCM config updates for the affected platforms are currently in the process of
being reviewed/merged into pmaports.

### Also see:
- [pmaports!3267](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3267)
- [pmaports!3268](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3268)
- [pmaports!3272](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3272)
- [pmaports!3273](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3273)
