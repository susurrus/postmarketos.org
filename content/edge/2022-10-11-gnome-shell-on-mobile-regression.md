title: "GNOME Shell on mobile breakage"
date: 2022-10-11
---

Currently GNOME Shell on mobile boots into an "Oh no! Something has gone wrong"
screen.

Related issue: [pma#1735](https://gitlab.com/postmarketOS/pmaports/-/issues/1735)
