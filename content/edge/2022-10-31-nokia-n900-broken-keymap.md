title: "Nokia N900: keyboard mapping broken"
date: 2022-10-31
---

With the upgrade to `xkeyboard-config 2.37` in postmarketOS, it is not possible to type in numbers and other symbols on the N900's hardware keyboard. As a result, the 20221027-0119 edge image is unusable out of the box, since you cannot enter the default password (147147), nor can you ssh in (disabled in official images).

A WIP fix is available in [pma!3596](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3596). Until new images are shipped containing the fix, it is advised that after writing the image to the SD card, you mount it in Linux, then overwrite the contents of `/usr/share/X11/xkb/symbols/nokia_vndr/rx-51` with the contents of [this file](https://gitlab.com/postmarketOS/pmaports/-/raw/b0ff7d1db2d836382bad369d6e8efbab95eedd43/device/community/device-nokia-n900/x11-keymap), ensuring the file name remains `rx-51` in the SD card.

Related issue: [pma#1775](https://gitlab.com/postmarketOS/pmaports/-/issues/1775)  
MR with fix: [pma!3596](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3596)  
Upstream MR and discussion : [fdo gitlab](https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/merge_requests/440)

