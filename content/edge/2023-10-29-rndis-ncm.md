title: "Default USB networking gadget changed to NCM"
date: 2023-10-29
---

With [MR !3670](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3670)
the default networking gadget on postmarketOS has changed from RNDIS (Remote
Network Driver Interface Specification) to NCM (Network Control Model). There
should be no noticeable change for most users.

The only known incompatibility is the absence of a NCM driver on Windows 10 and
lower. See the [Windows FAQ](https://wiki.postmarketos.org/wiki/Windows_FAQ) on
the postmarketOS wiki for more details. Windows 11 is **not** affected and
should continue to work as expected.

In case you want to continue using RNDIS for whatever reason, you can add the
following line to your `/etc/deviceinfo` file and run `sudo mkinitfs`:
```
deviceinfo_usb_network_function="rndis.usb0"
```

In case you experience any issues, please open an issue in the [pmaports issue
tracker](https://gitlab.com/postmarketOS/pmaports/-/issues) and let us know.
