title: "SDM845 devices occasionally just have a black screen on boot"
date: 2023-12-08
---

There is a ~50% chance that the display will fail to initialise during boot on
the OnePlus 6 since the 6.6 kernel upgrade. Other devices are suspected to
encounter this too (please let Caleb know on Matrix if you've hit this on a
different SDM845 device).

This seems to be due to a race condition in the kernel, the issue has been
[reported upstream here](https://gitlab.freedesktop.org/drm/msm/-/issues/46). As
a temporary workaround, enabling the UART console seems to resolve the issue and
cause the display to always initialise during boot.

This workaround has been enabled for the OnePlus 6(T), SHIFT6mq, and PocoPhone
F1 in {{MR|4610|pmaports}}. Please update your device, when the binary packages
are available!

You can also enable the workaround manually by adding the following to `/etc/deviceinfo`

```shell
# Temporary workaround for display init race condition
deviceinfo_kernel_cmdline_append="console=ttyMSM0,115200"
```

Don't forget to run `sudo mkinitfs` after modifying deviceinfo.
