title: "Possible issues with new GTK 4.0 renderer on some devices"
date: 2024-02-06
---

We have merged a GTK 4.0 Release Candidate (4.13.6) into pmaports which
changes the default renderer. The
[new](https://blog.gtk.org/2024/01/28/new-renderers-for-gtk/) default renderer
is a complete new implementation with clear benefits according to the
developers. However, as any new big code changes, there might be some issues.
If you experience any regressions, you can create a file containing
`GSK_RENDERER=gl` under `/etc/profile.d` to switch back to the old
renderer. In such case, it would be great if you
[open an issue](https://gitlab.com/postmarketOS/pmaports/-/issues/new) to let
us know.

Related: {{MR|4773|pmaports}}
