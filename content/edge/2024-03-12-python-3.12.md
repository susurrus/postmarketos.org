title: "postpone your upgrades: Alpine shipping Python 3.12"
date: 2024-04-12
---

Python 3.12 is landing in Alpine edge, woohoo! However this means the builders have a
lot of work to do to rebuild all the Python libraries and other related
packages, as a result the repos may be in an inconsistent state for a while. We
suggest postponing any upgrades for the next few hours (or maybe a day).

For the technically inclined, the builder status can be seen at <https://build.alpinelinux.org/>

<https://fosstodon.org/@ncopa/112257694554891368>

