title: "Devices using UEFI boot may be unable to boot"
date: 2024-05-16
---

For devices that use UEFI boot, ie where `/boot` is a `vfat` partition,
[this commit](https://gitlab.com/postmarketOS/pmaports/-/commit/bb6d7a05b3feffaaa4f3fe09b4d9a5ac939f41b8)
introduced a regression that causes the partition to not be mounted in some cases,
which then causes boot to fail with an "initramfs-extra not found" error.

Specifically, the list of devices affected are those which use the following kernels:

- `linux-postmarketos-grate`
- `linux-postmarketos-qcom-msm8916`
- `linux-ayn-odin`
- `linux-postmarketos-qcom-sc8180x`

[A fix](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5136) is being worked on.
Meanwhile, users of these devices are recommended to not install updates.

If you have already installed the update and have a device that fails to boot
with the "initramfs-extra not found" error, you can recover the installation by booting
a working image, `chroot`'ing into the original installation,
editing `/usr/share/initramfs/init_functions.sh` in this way:

```diff
-mount_opts="-t vfat $mount_opts,umask=0077,nosymfollow,codepage=437,iocharset=ascii"
+mount_opts="-t vfat $mount_opts,umask=0077,nosymfollow,codepage=437"
```

... and rebuilding the initramfs with `mkinitfs`
